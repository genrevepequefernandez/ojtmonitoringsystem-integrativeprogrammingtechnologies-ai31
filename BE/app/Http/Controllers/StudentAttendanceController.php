<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use App\Models\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentAttendanceController extends Controller
{
    public function index(){
        $data = Attendance::with('student')->where('date', Carbon::now('Asia/Manila')->toFormattedDateString())->get();
        // $date = Carbon::now('Asia/Manila')->toFormattedDateString();
        // return response()->json($date);
        return response()->json($data, 200);
    }

    public function timein(Request $request){
        $stud = Student::where('id', $request->student_id)->first();
        $record = Attendance::where('student_id', $request->student_id)->where('date', Carbon::now('Asia/Manila')->toFormattedDateString())->first();
        
        if(!empty($stud)){

            if(empty($record)){
                Attendance::create($request->all());
            }
            else {
                return response()->json(['msg' => 'Student has already timed in'], 200);
            }
            
            return response()->json(['msg' => 'Time in recorded successfully!'], 200);
        }
        else {
            return response()->json(['msg' => 'Student ID not found'], 404);
        }
    }

    public function timeout(Request $request){
        $stud = Student::where('id', $request->student_id)->first();
        $record = Attendance::where('student_id', $request->student_id)->where('date', Carbon::now('Asia/Manila')->toFormattedDateString())->first();
        
        if(!empty($stud)){

            if(empty($record)){
                Attendance::create($request->all());
            }
            else {
                if(empty($record->time_out)){
                    $record->update([
                        'time_out' => $request->time_in
                    ]);
                    return response()->json(['msg' => 'Student time out has been recorded'], 200);
                } 
                else {   
                    return response()->json(['msg' => 'Student has already timed out'], 200);
                }    
            }

            return response()->json(['msg' => 'Time out recorded successfully!'], 200);
        }
        else {
            return response()->json(['msg' => 'Student ID not found'], 404);
        }
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\AdviserAccount;
use App\Models\StudentAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class StudentAccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt', ['except' => ['login']]);
    }

    public function login(Request $request)
    {
        $credentials = [
            'student_id' => $request->email,
            'password' =>$request->password,
        ];

        if (!$token = auth()->guard('student-api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }

    public function logout()
    {
        JWTAuth::invalidate(Request()->token);
        auth()->logout();
        return response()->json(['message' => 'User logged out successfully!']);
    }

    protected function respondWithToken($token)
    {
        $id = auth('student-api')->user();
        $user = StudentAccount::with('student:id,first_name,middle_name,last_name', 'student.section:id,section', 'student.adviser')->where('id', $id->id)->first();

        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('student-api')->factory()->getTTL() * 60,
            'user' => $user,
        ])->header('Authorization: Bearer ', $token);
    }

    public function payload()
    {
        return auth()->payload();
    }

    public function me()
    {
        return response()->json(StudentAccount::with(['student', 'student.gender:id,gender'])->find(Auth::id()));
    }
}

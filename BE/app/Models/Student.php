<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'first_name',
        'middle_name',
        'last_name',
        'section_id',
        'image',
        'gender_id',
        'school_year_id',
        'semester_id',
        'adviser_id',
    ];

    public function section(){
        return $this->belongsTo(Section::class, 'section_id', 'id');
    }

    public function gender(){
        return $this->belongsTo(Gender::class, 'gender_id', 'id');
    }

    public function adviser(){
        return $this->belongsTo(Adviser::class, 'adviser_id', 'id');
    }

    public function attendance(){
        return $this->hasMany(Attendance::class, 'student_id', 'id');
    }
}

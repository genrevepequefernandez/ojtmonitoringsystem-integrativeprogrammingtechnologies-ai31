<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Adviser extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'department',
        'image',
        'gender_id',
    ];

    public function gender(){
        return $this->belongsTo(Gender::class, 'gender_id', 'id');
    }
}

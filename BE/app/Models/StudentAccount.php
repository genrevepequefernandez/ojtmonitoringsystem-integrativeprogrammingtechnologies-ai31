<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class StudentAccount extends Authenticatable implements JWTSubject
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'email',
        'password'
    ];

    protected $hidden = [
        'password',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function student(){
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }
}

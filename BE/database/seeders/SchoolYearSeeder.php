<?php

namespace Database\Seeders;

use App\Models\SchoolYear;
use Illuminate\Database\Seeder;

class SchoolYearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schoolyear = [
            ['school_year' => '2021 - 2022'],
            ['school_year' => '2022 - 2023'],
            ['school_year' => '2023 - 2024'],
            ['school_year' => '2025 - 2026'],
            ['school_year' => '2027 - 2028'],
            ['school_year' => '2029 - 2030'],
            ['school_year' => '2030 - 2031'],
            ['school_year' => '2031 - 2032'],
            ['school_year' => '2032 - 2033'],
            ['school_year' => '2034 - 2035'],
        ];

        foreach ($schoolyear as $s) {
            SchoolYear::create($s);
        }
    }
}

import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/pages/Main/Login'
import Admin from '../components/pages/Teacher/Admin'
import Settings from '../components/pages/Teacher/Settings'
import Students from '../components/pages/Teacher/Student'
import Attendance from '../components/pages/Teacher/Attendance'
import student_attendance from '../components/pages/Student/Attendance'
import Activities from '../components/pages/Student/Activities'
import Student_Settings from '../components/pages/Student/Settings'


Vue.use(VueRouter)

 const routes = [
   {
     path: '/',
     name: 'Main',
     component: Login,
     meta: { hasUser: true }
   },
   {
     path: '/signup/teacher',
     name: 'Teacher Signup',
     component: () => import('@/components/pages/Main/Signup'),
     meta: { hasUser: true }
   },
   {
    path: '/time/record',
    name: 'Time Record',
    component: () => import('@/components/pages/Main/TimeRecord'),

  },
  {
    path: '/student',
    name: 'StudentAccount',
    component: () => import('@/components/pages/Student/Student'),
    meta: { isStudent: true },
    children: [
      {
        path: 'settings',
        name: 'settings',
        components: {
          settings: Student_Settings
        }
      },
      {
        path: 'activities',
        name: 'activities',
        components: {
          activities: Activities
        }
      },
      {
        path: 'attendance',
        name: 'attendance',
        components: {
          attendance: student_attendance
        }
      },
    ]
  },
  {
     path: '/admin',
     name: 'Management',
     component: Admin,
     meta: { requiresLogin: true },
     children: [
       {
         path: 'settings',
         name: 'settings',
         components: {
           settings: Settings
         }
       },
       {
         path: 'students',
         name: 'students',
         components: {
           students: Students
         }
       },
       {
         path: 'attendance',
         name: 'attendance',
         components: {
           attendance: Attendance
         }
       },
     ]
   },
  {
    path: '*',
    name: 'NotFound',
    component: () => import('@/components/pages/Main/NotFound'),
  },
]

const router = new VueRouter({
   mode: 'history',
   base: process.env.BASE_URL,
   routes
})


router.beforeEach((to, from, next) => {
  
	if (to.matched.some((record) => record.meta.requiresLogin) && !localStorage.getItem("auth") && !localStorage.getItem("logged_in_student")) {
		next({ name: "Main" });
  } else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem("auth") && !localStorage.getItem("logged_in_student")) {
		next({ name: "Management" });
  } else if(to.matched.some((record) => record.meta.hasUser) && localStorage.getItem("logged_in_student")){
		next({ name: "StudentAccount" });
  } else if(to.matched.some((record) => record.meta.isStudent) && !localStorage.getItem("logged_in_student")){
		next({ name: "Main" });
  } else {
		next();
	}
  
});

export default router